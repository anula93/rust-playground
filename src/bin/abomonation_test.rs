extern crate abomonation;
extern crate rust_playground;

use std::vec::Vec;
use abomonation::{encode, decode};

use rust_playground::Query;

fn main() {
    let mut bytes = Vec::<u8>::new();
    let s = Query{field: "Hello World\n".to_string()};
    unsafe { encode(&s, &mut bytes); }
    if let Some((result, remaining)) = unsafe { decode::<Query>(&mut bytes) } {
        println!("Res: {:?}", result);
        assert!(remaining.len() == 0);
    }
}
