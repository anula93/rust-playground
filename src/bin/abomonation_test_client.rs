extern crate abomonation;
extern crate rust_playground;

use std::net::TcpStream;
use std::io::Read;
use std::vec::Vec;
use abomonation::decode;

use rust_playground::Query;

fn main() {
    let mut stream = TcpStream::connect("127.0.0.1:34254").unwrap();

    let mut bytes = Vec::<u8>::new();
    let _ = stream.read_to_end(&mut bytes);
    if let Some((result, remaining)) = unsafe { decode::<Query>(&mut bytes) } {
        println!("{:?}", result);
        assert!(remaining.len() == 0);
    }
}
