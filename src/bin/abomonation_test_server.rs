extern crate abomonation;
extern crate rust_playground;

use std::io::Write;
use std::net::TcpListener;
use std::thread;
use abomonation::encode;

use rust_playground::Query;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:34254").unwrap();
    println!("listening started, ready to accept");
    for stream in listener.incoming() {
        thread::spawn(|| {
            let s = Query{field: "Hello World\n".to_string()};
            let mut bytes = Vec::new();
            unsafe { encode(&s, &mut bytes); }
            let mut stream = stream.unwrap();
            stream.write_all(bytes.as_slice()).unwrap();
        });
    }
}
