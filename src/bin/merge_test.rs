extern crate timely;

use timely::dataflow::operators::{Input, Concat, Inspect};
use timely::Configuration;

fn main() {
    timely::execute(Configuration::Thread, |root| {
        let (mut input1, mut input2) =
            root.dataflow::<u32, _, _>(|scope| {
                              let (input1, stream1) = scope.new_input();
                              let (input2, stream2) = scope.new_input();
                              stream2.concat(&stream1).inspect(|x| println!("seen: {:?}", x));
                              (input1, input2)
                          });
        for i in 0..10 {
            input1.send(1);
            input1.advance_to(i+1);
            input2.send(2);
            input2.advance_to(i+1);
            root.step();
        }

    }).unwrap();
}
