extern crate timely;

use timely::dataflow::operators::{Input, Inspect};
use timely::dataflow::operators::exchange::Exchange;

fn main() {

    // initializes and runs a timely dataflow computation
    timely::execute_from_args(std::env::args(), |root| {

        // create a new input and inspect its output
        let mut input = root.dataflow(|scope| {
            let (input, stream) = scope.new_input();
            stream.exchange(|&x| x).inspect(|x| println!("hello {}", x));
            input
        });

        // introduce data and watch!
        for round in 0..10 {
            input.send(round);
            input.advance_to(round + 1);
            root.step();
        }

        // seal the input
        input.close();

        // finish off any remaining work
        while root.step() {}

    })
            .unwrap();
}
