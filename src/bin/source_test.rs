extern crate timely;

use timely::dataflow::operators::Inspect;
use timely::dataflow::operators::operator::source;
use timely::dataflow::Scope;

fn main() {

    timely::example(|scope| {

        source(scope, "Source", |capability| {
            let mut cap = Some(capability);
            move |output| {

                let mut done = false;
                if let Some(cap) = cap.as_mut() {
                    // get some data and send it.
                    let mut time = cap.time().clone();
                    output.session(&cap).give(cap.time().inner);

                    // downgrade capability.
                    time.inner += 1;
                    *cap = cap.delayed(&time);
                    done = time.inner > 20;
                }

                if done {
                    cap = None;
                }
            }
        })
                .inspect(|x| println!("number: {:?}", x));
    });
}
