#[macro_use]
extern crate abomonation;
use abomonation::Abomonation;

#[derive(Clone, Debug)]
pub struct Query {
    pub field: String,
}

unsafe_abomonate!(Query: field);
